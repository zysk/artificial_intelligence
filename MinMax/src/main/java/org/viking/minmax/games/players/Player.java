package org.viking.minmax.games.players;

import org.viking.minmax.games.play.Move;
import org.viking.minmax.model.Board;

import java.util.List;

public interface Player {

    void makeMove(Move move);
    void undoMove(Move move);
    List<Move> getAvailable();
    int eval(Board state);
    Object clone();
}
