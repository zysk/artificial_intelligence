package org.viking.ColourfullGraph;

import java.util.ArrayList;

import junit.framework.TestCase;

import org.viking.structures.graph.Edge;

public class EdgeTest extends TestCase {

	ArrayList<Edge> edges = new ArrayList<Edge>();
	public void createEdgeList(){
		edges.add(new Edge(0, 0));
		edges.add(new Edge(0, 1));
		edges.add(new Edge(0, 2));
		edges.add(new Edge(1, 2));
	}
	
	public void testEqualsObject() {
		Edge e1 = new Edge(1, 0);
		Edge e2 = new Edge(0, 1);
		assertTrue(e1.equals(e2));
	}

	public void testEqualsObject2() {
		Edge e1 = new Edge(1, 1);
		Edge e2 = new Edge(1, 1);
		assertTrue(e1.equals(e2));
	}
	
	public void testEqualsObject3() {
		Edge e1 = new Edge(4, 1);
		Edge e2 = new Edge(1, 3);
		assertFalse(e1.equals(e2));
	}
	
	public void testContainsList(){
		createEdgeList();
		Edge testCase = new Edge(0,0);
		assertTrue(edges.contains(testCase));
	}
	
	public void testContainsList1(){
		createEdgeList();
		Edge testCase = new Edge(0,1);
		assertTrue(edges.contains(testCase));
	}
	
	public void testContainsList2(){
		createEdgeList();
		Edge testCase = new Edge(0,2);
		assertTrue(edges.contains(testCase));
	}
	

	
	
}

