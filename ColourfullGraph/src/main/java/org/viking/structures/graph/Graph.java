package org.viking.structures.graph;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.viking.filemanagement.GraphImporter;
import org.viking.runtime.Main;

public class Graph {

	private List<Edge> edges;
	private int vertexCount;
	
	public Graph(String graphPath){
		 edges = new ArrayList<Edge>();
		 vertexCount = 0;
		 importGraph(graphPath);
	}
	
	public List<Edge> getEdges(){
		return edges;
	}
	
	public void addEdge(int beginVertex, int endVertex){
		edges.add(new Edge(beginVertex, endVertex));
		if(beginVertex > vertexCount)
			vertexCount = beginVertex;
		if(endVertex > vertexCount)
			vertexCount = endVertex;
	}	

	public int getVertexCount() {
		return vertexCount;
	}
	
	private void importGraph(String graphPath){
		GraphImporter importer = new GraphImporter();
		File graphFile = FileUtils.toFile(Main.class.getResource("/graphs/" + graphPath + ".col.txt"));
		importer.importGraph(graphFile, this);	
	}

	
}
