package org.viking.csp.algorithms;

import org.viking.csp.model.AbstractQueen;

public class BackTracking extends AbstractQueen {

	public BackTracking(int SIZE) {
		super(SIZE);
	}

	@Override
	public boolean run(int column) {
		int row = 0;
		boolean success = false;

		while (row < SIZE && !success && column < SIZE) {

			iterationCount++;
			if (isConflict(row, column)) {
				row++;
			} else {
				putQueen(row, column);
				success = run(column + 1);

				if (column == SIZE - 1) {
					resultCount++;
				}

				if (!success) {
					removeQueen(row, column);
					row++;
				}
			}
		}

		return success;
	}

	@Override
	public void putQueen(int row, int column) {
		board[row][column] = true;
	}

	public boolean isConflict(int row, int col) {
		if (isConflictOnDownDiagonal(row, col)
				|| isConflictOnUpDiagonal(row, col)
				|| isConflictOnRow(row, col)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isConflictOnDownDiagonal(int row, int col) {
		while (col > 0 && row < SIZE - 1) {
			if (board[++row][--col])
				return true;
		}
		return false;
	}

	public boolean isConflictOnUpDiagonal(int row, int col) {
		while (col > 0 && row > 0) {
			if (board[--row][--col])
				return true;
		}
		return false;
	}

	public boolean isConflictOnRow(int row, int col) {
		for (int i = col - 1; i >= 0; i--) {
			if (board[row][i])
				return true;
		}
		return false;
	}

}
