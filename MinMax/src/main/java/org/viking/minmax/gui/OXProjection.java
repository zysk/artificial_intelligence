package org.viking.minmax.gui;

import org.viking.minmax.games.play.Game;
import org.viking.minmax.gui.listeners.OXListener;
import org.viking.minmax.model.Figure;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;


public class OXProjection extends JPanel {



    private Game game;
    private Figure[][] board;
    private JLabel[][] projectionLabels;
    private GridLayout layout;

    public OXProjection(Game game) {

        startOXProjection(game);
    }

    private void createLabels() {
        JLabel myLabel;

        for (int i = 0; i < projectionLabels.length; i++) {
            for (int j = 0; j < projectionLabels.length; j++) {
                myLabel = new JLabel("", SwingConstants.CENTER);
                myLabel.setVisible(true);
                myLabel.setOpaque(true);
                myLabel.setBackground(Color.WHITE);
                myLabel.setFont(new Font("Calibri", Font.BOLD, 24));
                projectionLabels[i][j] = myLabel;
                add(myLabel);
            }
        }
    }

    public void projectBoard() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {

                if (board[i][j] != null) {
                    projectionLabels[i][j].setText(board[i][j].toString());
                }
            }
        }
        this.repaint();
    }


    public void repaintAllLabels() {

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                projectionLabels[i][j].repaint();
            }
        }

        this.repaint();
    }

    public void addListeners() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {

                if (board[i][j] == null) {
                    projectionLabels[i][j].addMouseListener(new OXListener(i, j, this));
                }
            }
        }
    }

    public void removeMouseListenerFormPanel(int i, int j, MouseAdapter adapter) {
        projectionLabels[i][j].removeMouseListener(adapter);
        repaintAllLabels();
    }

    public void startOXProjection(Game game) {
        prepareProjection(game);
        setVisible(true);
        createLabels();
        addListeners();
        projectBoard();
        repaintAllLabels();

        this.invalidate();
        this.validate();
    }

    public void prepareProjection(Game game) {

        this.removeAll();
        this.game = game;
        board = game.getState().getBoard();
        this.layout = new GridLayout(board.length, board.length, 2, 2);
        projectionLabels = new JLabel[board.length][board.length];
        setLayout(layout);
        setVisible(true);
        setBackground(Color.DARK_GRAY);
    }

    public Game getGame() {
        return game;
    }

}
