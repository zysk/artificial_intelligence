package org.viking.csp.model;

import java.util.Arrays;

public abstract class AbstractQueen {

	protected final int SIZE;
	protected boolean[][] board;
	protected int resultCount;
	protected int iterationCount;
	private long duration;
	
	public AbstractQueen(int SIZE){
		this.SIZE = SIZE;
		board = new boolean[SIZE][SIZE];
		cleanBoard();
		resultCount = 0;
		iterationCount = 0;
		duration = System.currentTimeMillis();
	}
	
	public boolean run(){
		boolean result = run(0);
		duration = System.currentTimeMillis() - duration;
		return result;
	}
	
	protected abstract boolean run(int column);
	protected abstract void putQueen(int row, int column);
	
	public void removeQueen(int row, int column) {
		board[row][column] = false;
	}
	
	private void cleanBoard(){
		for(int i = 0; i < board.length; i++){
			Arrays.fill(board[i], false);
		}
	}
	
	public void printBoard() {
		System.out.println("----BOARD----");
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board.length; col++) {
				System.out.print(board[row][col]==true ? "1 " : "0 ");
			}
			System.out.println();
		}
	}
	
	public int getResultCount() {
		return resultCount;
	}

	public int getIterationCount() {
		return iterationCount;
	}
	
	public long getDuration() {
		return duration;
	}
}
