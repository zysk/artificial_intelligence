package org.viking.minmax.games.ox;

import org.viking.minmax.model.Figure;


public enum OXFigure implements Figure{

    X, O;

}
