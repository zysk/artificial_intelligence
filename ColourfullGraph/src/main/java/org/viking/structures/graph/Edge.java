package org.viking.structures.graph;

public class Edge {

	private int beginVertex, endVertex;

	public Edge(int beginVertex, int endVertex) {
		this.beginVertex = beginVertex;
		this.endVertex = endVertex;
	}

	public int getBeginVertex() {
		return beginVertex;
	}

	public int getEndVertex() {
		return endVertex;
	}

	@Override
	public boolean equals(Object e) {
		Edge edge = (Edge) e;
		if ((edge.getBeginVertex() == this.getEndVertex() && edge
				.getEndVertex() == this.getBeginVertex())
				|| (edge.getBeginVertex() == this.getBeginVertex() && edge
						.getEndVertex() == this.getEndVertex())) {
			return true;
		}
		return false;
	}
}
