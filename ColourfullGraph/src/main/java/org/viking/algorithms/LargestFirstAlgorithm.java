package org.viking.algorithms;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.viking.filemanagement.LargestFirstResultsSaver;
import org.viking.structures.graph.Edge;
import org.viking.structures.graph.Graph;

public class LargestFirstAlgorithm {

	private int[] vertexes;
	private int[] colors;
	private List<Integer> coloruedVertexes = new LinkedList<Integer>();
	Graph graph;
	int colorCount;

	public LargestFirstAlgorithm(Graph g) {
		this.graph = g;
		colorCount = 0;
		vertexes = new int[graph.getVertexCount()];
		colors = new int[graph.getVertexCount()];
		Arrays.fill(colors, -1);
		countVertexesFrequency();
	}

	private void countVertexesFrequency() {
		for (Edge e : graph.getEdges()) {
			vertexes[e.getBeginVertex() - 1]++;
			vertexes[e.getEndVertex() - 1]++;
		}
	}

	public int run(String logFile) {
		int maxValueIndex;
		LargestFirstResultsSaver.startSaving(logFile);
		long time = System.currentTimeMillis();
		
		while (coloruedVertexes.size() < vertexes.length) {
			maxValueIndex = findMaxValueIndex();
			coloruedVertexes.add(maxValueIndex);
			colorVertex(maxValueIndex);
		}
		
		LargestFirstResultsSaver.saveData(colorCount, (int) (System.currentTimeMillis()-time));
		LargestFirstResultsSaver.endSaving();
		return colorCount;
	}

	public int findMaxValueIndex() {
		int maxValue = Integer.MIN_VALUE;
		int maxValueIndex = 0;
		for (int i = 0; i < vertexes.length; i++) {
			if (!coloruedVertexes.contains(i)) {
				if (vertexes[i] > maxValue) {
					maxValue = vertexes[i];
					maxValueIndex = i;
				}
			}
		}
		return maxValueIndex;
	}

	public void colorVertex(int maxValueIndex) {
		boolean founded = false;
		int color = 1;
		
		while (!founded) {
			if(isConfilct(maxValueIndex, color)){
				color++;
			}else{
				colors[maxValueIndex] = color;
				founded = true;
				
				if(colorCount < color){
					colorCount = color;
				}
			}
		}
	}

	private boolean isConfilct(int maxValueIndex, int color) {
		for (Edge e : graph.getEdges()) {
			if(e.getBeginVertex() == (maxValueIndex +1) && colors[e.getEndVertex() -1] == color){
				return true;
			}
			if(e.getEndVertex() == (maxValueIndex +1) && colors[e.getBeginVertex() -1] == color){
				return true;
			}
		}
		return false;
	}

}
