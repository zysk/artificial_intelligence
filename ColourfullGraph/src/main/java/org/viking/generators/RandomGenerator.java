package org.viking.generators;

import java.util.Random;

public class RandomGenerator {

	private static final Random generator = new Random();
	
	public static int getRandomColor(int scope){
		return generator.nextInt(scope);
	}
	
	public static double getRandomProbability(){
		return generator.nextDouble();
	}
	
	
	public static int getRandomInteger(int scope){
		return generator.nextInt(scope);
	}
}
