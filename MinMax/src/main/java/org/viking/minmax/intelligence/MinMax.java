package org.viking.minmax.intelligence;

import org.viking.minmax.games.play.Game;
import org.viking.minmax.games.play.Move;
import org.viking.minmax.games.players.Player;
import org.viking.minmax.intelligence.comparators.MinMaxComparator;
import org.viking.minmax.model.Board;

import java.util.Iterator;

public class MinMax {

    private int depth = 1;
    private Board state;
    private Player original;

    public MinMax(int depth) {
        this.depth = depth;
    }

    public Move bestMove(Board state, Player player, Player opponent){
        this.state = (Board) state.clone();
        this.original = player;

        MoveEvaluation me = minmax(depth, MinMaxComparator.MAX, player, opponent);
        System.out.println(me.getEval());
        return me.getMove();
    }

    private MoveEvaluation minmax(int dep, MinMaxComparator comp, Player player, Player opponent){

        Iterator<Move> it = player.getAvailable().iterator();
        if(dep == 0 || !it.hasNext()){
            return new MoveEvaluation(original.eval(state));
        }

        MoveEvaluation best = new MoveEvaluation(comp.initalValue());

        while(it.hasNext()){
            Move move = it.next();

            player.makeMove(move);

            MoveEvaluation me = minmax(dep - 1, comp.opposite(), opponent, player);

            player.undoMove(move);

            if(comp.compare(best.getEval(), me.getEval()) < 0){
                best = new MoveEvaluation(move, me.getEval());
            }
        }

        return best;
    }

}
