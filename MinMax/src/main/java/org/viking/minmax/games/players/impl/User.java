package org.viking.minmax.games.players.impl;


import org.viking.minmax.games.ox.OXFigure;
import org.viking.minmax.games.ox.OXState;
import org.viking.minmax.games.play.Move;
import org.viking.minmax.games.players.Player;
import org.viking.minmax.model.Board;
import org.viking.minmax.model.Figure;
import org.viking.minmax.utils.BoardUtils;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.*;

public class User implements Player {

    protected Board state;
    protected Figure figure;

    public User(Board state, Figure figure) {
        this.state = state;
        this.figure = figure;
    }

    public void makeMove(Move move) {
        state.putFigure(figure, move.getX(), move.getY());
        state.switchPlayer();
    }

    public void undoMove(Move move) {
        state.removeFigure(move.getX(), move.getY());
    }

    public List<Move> getAvailable() {
        List<Move> moves = new LinkedList<Move>();
        for (int i = 0; i < state.getSIZE(); i++) {
            for (int j = 0; j < state.getSIZE(); j++) {
                if (state.getBoard()[i][j] == null) {
                    moves.add(new Move(i, j));
                }
            }
        }
        return moves;
    }

    public int eval(Board state) {
//        List<Integer> list = new ArrayList<Integer>();
//        list.add(evaluateCols(state));
//        list.add(evaluateRows(state));
//        list.add(evaluateDownDiagonals(state));
//        list.add(evaluateUpDiagonals(state));
//        return Collections.max(list);
        return centerEvaluation();
    }

    private int evaluateUpDiagonals(Board state) {
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < state.getSIZE(); i++) {
            for (int c = 0; c < state.getSIZE(); c++) {
                list.add(evalueteFragment(BoardUtils.getSubarrayFromUpDia(state.getBoard(), c, i, state.getCheckSize())));
            }
        }
        return Collections.max(list);
    }

    private int evaluateDownDiagonals(Board state) {
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < state.getSIZE(); i++) {
            for (int c = 0; c < state.getSIZE(); c++) {
                list.add(evalueteFragment(BoardUtils.getSubarrayFromDownDia(state.getBoard(), c, i, state.getCheckSize())));
            }
        }
        return Collections.max(list);
    }


    private int evaluateRows(Board state) {
        List<Integer> list = new ArrayList<Integer>();
        for (int row = 0; row < state.getSIZE(); row++) {
            for (int i = 0; i < state.getSIZE() - state.getCheckSize() + 1; i++) {
                list.add(evalueteFragment(Arrays.copyOfRange(state.getBoard()[row], i, i + state.getCheckSize())));
            }
        }
        return Collections.max(list);
    }

    private int evaluateCols(Board state) {
        List<Integer> list = new ArrayList<Integer>();
        for (int col = 0; col < state.getSIZE(); col++) {
            for (int i = 0; i < state.getSIZE() - state.getCheckSize() + 1; i++) {
                list.add(evalueteFragment(BoardUtils.getSubarrayFromCol(state.getBoard(), col, i, state.getCheckSize())));
            }
        }
        return Collections.max(list);
    }

    private int evalueteFragment(Figure[] figures) {
        int figCount = 0;
        int nullCount = 0;
        int oppCount = 0;
        for (int i = 0; i < figures.length; i++) {
            if (figures.length == state.getCheckSize()) {
                if (figures[i] == figure) {
                    figCount++;
                }else if (figures[i] == null) {
                    nullCount++;
                }else{
                    return 0;
                }
            } else {
                return 0;
            }
        }

        return figCount + (state.getCheckSize() - nullCount);
    }

    private int centerEvaluation(){
        int centerCount = 0;
        for(int i = 0; i < state.getBoard().length; i++){
            for(int j = 0; j < state.getBoard().length; j++){
                if(state.getBoard()[i][j] == figure){
                    centerCount += Math.min(i, j) +1;
                }
            }
        }
        return centerCount;
    }

    @Override
    public Object clone() {
        return new User((Board) state.clone(), figure);
    }
}
