package org.viking.filemanagement;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class LargestFirstResultsSaver {

	static BufferedWriter bw = null;

	public static void startSaving(String fileName) {
		File resultFile = automatizeFilename("LF_" + fileName + "-0");

		FileWriter fw;

		try {

			fw = new FileWriter(resultFile.getAbsoluteFile());
			bw = new BufferedWriter(fw);
			bw.write("<algorithm>\n ");
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}



	public static void endSaving() {
		try {
			bw.write("</algorithm>");
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void saveData(int colors, int time) {
		try {
			bw.write("\t<colors>");
			bw.write(colors+"");
			bw.write("</colors>\n");
			bw.write("\t<time>");
			bw.write(time+"");
			bw.write("</time>\n");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static File automatizeFilename(String fileName) {
		File resultFile = new File("src/main/resources/results/" + fileName + ".xml");

		while (resultFile.exists()) {
			String[] table = fileName.split("-");
			int nr = Integer.parseInt(table[1]) + 1;
			fileName = (table[0] + "-" + nr);
			resultFile = new File("src/main/resources/results/" + fileName + ".xml");
		}

		try {
			resultFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return resultFile;

	}

}
