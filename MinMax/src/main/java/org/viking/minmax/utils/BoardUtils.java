package org.viking.minmax.utils;


import org.viking.minmax.model.Figure;

public class BoardUtils {


    public static Figure[] getSubarrayFromCol(Figure[][] board, int col, int from, int length) {
        Figure[] sub = new Figure[length];

        for (int i = 0; i < length && from < board.length; i++) {
            sub[i] = board[from++][col];
        }

        return sub;
    }

    public static Figure[] getSubarrayFromUpDia(Figure[][] board, int col, int row, int length) {
        Figure[] sub = new Figure[length];

        for (int i = 0; row >= 0 && i < length && col < board.length; i++) {
            sub[i] = board[row--][col++];
        }

        return sub;
    }

    public static Figure[] getSubarrayFromDownDia(Figure[][] board, int col, int row, int length) {
        Figure[] sub = new Figure[length];

        for (int i = 0; row >= 0 && i < length && col >= 0; i++) {
            sub[i] = board[row--][col--];
        }
        return sub;
    }

}
