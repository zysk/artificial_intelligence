package org.viking.minmax.games.ox;

import org.viking.minmax.games.players.Player;
import org.viking.minmax.model.Board;
import org.viking.minmax.model.Figure;
import org.viking.minmax.utils.BoardUtils;

import java.util.Arrays;


public class OXState implements Board {


    private boolean playerSwitch;
    private Figure[][] board;
    private int SIZE;
    private int checkSize;

    public OXState() {
        board = new Figure[3][3];
        this.SIZE = 3;
        this.checkSize = 3;
        playerSwitch = true;
    }

    public OXState(int size, int check) {
        board = new Figure[size][size];
        this.SIZE = size;
        setCheckSize(check);
        playerSwitch = true;
    }

    public boolean getPlayerSwitch(){
        return playerSwitch;
    }

    public void switchPlayer(){
        playerSwitch = !playerSwitch;
    }


    public int getCheckSize() {
        return checkSize;
    }

    public void setBoard(Figure[][] board) {
        this.board = board;
    }

    public void setCheckSize(int checkSize) {
        this.checkSize = checkSize;
    }

    public int getSIZE() {
        return SIZE;
    }

    public void putFigure(Figure figure, int row, int column) {
        board[row][column] = figure;
    }

    public void removeFigure(int row, int column) {
        board[row][column] = null;
    }

    public Figure[][] getBoard() {
        return board;
    }

    public boolean hasEmptyFields() {
        int counter = 0;
        for (int i = 0; i < getSIZE(); i++) {
            for (int j = 0; j < getSIZE(); j++) {
                if (board[i][j] == null)
                    counter++;
            }
        }
        return counter != 0;
    }

    public int getOCount() {
        int counter = 0;
        for (int i = 0; i < getSIZE(); i++) {
            for (int j = 0; j < getSIZE(); j++) {
                if (getBoard()[i][j] == OXFigure.O)
                    counter++;
            }
        }
        return counter;
    }

    public int getXCount() {
        int counter = 0;
        for (int i = 0; i < getSIZE(); i++) {
            for (int j = 0; j < getSIZE(); j++) {
                if (getBoard()[i][j] == OXFigure.X)
                    counter++;
            }
        }
        return counter;
    }


    public boolean isOver() {
        return checkRowsAndColumns() || checkDiagonals() || !hasEmptyFields();
    }

    private boolean checkRowsAndColumns() {
        for (int i = 0; i < getSIZE(); i++) {
            if (checkRow(i) || checkCol(i)) {
                return true;
            }
        }
        return false;
    }

    private boolean checkDiagonals() {
        if (checkDownDiagonal() || checkUpDiagonal()) {
            return true;
        }
        return false;
    }


    private boolean checkDownDiagonal() {
        for (int i = 0; i < getSIZE(); i++) {
            for (int c = 0; c < getSIZE(); c++) {
                if (checkFragment(BoardUtils.getSubarrayFromDownDia(getBoard(), c, i, getCheckSize())))
                    return true;
            }
        }
        return false;
    }

    private boolean checkUpDiagonal() {
        for (int i = 0; i < getSIZE(); i++) {
            for (int c = 0; c < getSIZE(); c++) {
                if (checkFragment(BoardUtils.getSubarrayFromUpDia(getBoard(), c, i, getCheckSize())))
                    return true;
            }
        }
        return false;
    }

    private boolean checkCol(int col) {
        for (int i = 0; i < getSIZE() - getCheckSize() + 1; i++) {
            if (checkFragment(BoardUtils.getSubarrayFromCol(getBoard(), col, i, getCheckSize())))
                return true;
        }
        return false;
    }

    private boolean checkRow(int row) {
        for (int i = 0; i < getSIZE() - getCheckSize() + 1; i++) {
            if (checkFragment(Arrays.copyOfRange(getBoard()[row], i, i + getCheckSize())))
                return true;
        }
        return false;
    }


    private boolean checkFragment(Figure[] figures) {
        int oCounter = 0;
        int xCounter = 0;

        for (int i = 0; i < figures.length; i++) {
            if (figures[i] == OXFigure.X) {
                xCounter++;
            }
            if (figures[i] == OXFigure.O) {
                oCounter++;
            }
        }

        if (oCounter == getCheckSize() || xCounter == getCheckSize()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Object clone() {
        OXState clone = new OXState(this.getSIZE(), this.getCheckSize());
        clone.setBoard(this.getBoard().clone());
        return clone;
    }
}
