package org.viking.minmax.intelligence.comparators;

/**
 * Created with IntelliJ IDEA.
 * User: Viking
 * Date: 08.05.13
 * Time: 21:12
 * To change this template use File | Settings | File Templates.
 */
public class MaxComparator implements MinMaxComparator {
    @Override
    public int initalValue() {
        return Integer.MIN_VALUE;
    }

    @Override
    public MinMaxComparator opposite() {
        return MinMaxComparator.MIN;
    }

    @Override
    public int compare(Integer o1, Integer o2) {
        if(o1 > o2) return 1;
        if(o1 == o2) return 0;
        return -1;
    }
}
