package org.viking.minmax.intelligence.comparators;

import java.util.Comparator;

public interface MinMaxComparator extends Comparator<Integer> {

    MinMaxComparator MIN = new MinComparator();
    MinMaxComparator MAX = new MaxComparator();

    int initalValue();
    MinMaxComparator opposite();

}
