package org.viking.exceptions;

public class AlgorithmFoundSolutionException extends Exception {

	private static final long serialVersionUID = 1L;

	public AlgorithmFoundSolutionException(String message) {
		super(message);
	}

	public AlgorithmFoundSolutionException() {
		super();
	}

}