package org.viking.ColourfullGraph;

import org.viking.algorithms.model.Chromosome;
import org.viking.algorithms.model.Population;
import org.viking.structures.graph.Graph;

import junit.framework.TestCase;

public class PopulationTest extends TestCase {

	public void testTournamentSelection() {
		Population population = new Population(new Graph("anna"), 20, 11);
		population.fillPopulationWithRandomChromosomes();
		Chromosome x1 = population.tournamentSelection(5);
		System.out.println(x1 + ", " + x1.numberOfFaliures());
	}

}
