package org.viking.runtime;

import org.viking.algorithms.GeneticAlgorithm;
import org.viking.algorithms.LargestFirstAlgorithm;
import org.viking.structures.graph.Graph;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String gr = "homer";
		Graph g = new Graph(gr);
		GeneticAlgorithm ga = new GeneticAlgorithm(g, 13);
		ga.setAlgorithmParameters(1000, 2000, 0.3, 0.001, 5);
		ga.run(gr);
	}
	
	public static void automatize2(){
		String gr = "anna";
		Graph g = new Graph(gr);
		for (int i = 0; i < 5; i++) {
			GeneticAlgorithm ga = new GeneticAlgorithm(g, 11);
			ga.setAlgorithmParameters(100, 1000, 0.3, 0.01, 5);
			ga.run(gr + "_p_100");
			ga = new GeneticAlgorithm(g, 11);
			ga.setAlgorithmParameters(500, 1000, 0.3, 0.01, 5);
			ga.run(gr + "_p_500");
			ga = new GeneticAlgorithm(g, 11);
			ga.setAlgorithmParameters(1000, 1000, 0.3, 0.01, 5);
			ga.run(gr + "_p_1000");
			ga = new GeneticAlgorithm(g, 11);
			ga.setAlgorithmParameters(3000, 1000, 0.3, 0.01, 5);
			ga.run(gr + "_p_3000");
		}
		for (int i = 0; i < 5; i++) {
			GeneticAlgorithm ga = new GeneticAlgorithm(g, 11);
			ga.setAlgorithmParameters(1000, 100, 0.3, 0.01, 5);
			ga.run(gr + "_g_100");
			ga = new GeneticAlgorithm(g, 11);
			ga.setAlgorithmParameters(1000, 500, 0.3, 0.01, 5);
			ga.run(gr + "_g_500");
			ga = new GeneticAlgorithm(g, 11);
			ga.setAlgorithmParameters(1000, 1000, 0.3, 0.01, 5);
			ga.run(gr + "_g_1000");
			ga = new GeneticAlgorithm(g, 11);
			ga.setAlgorithmParameters(1000, 5000, 0.3, 0.01, 5);
			ga.run(gr + "_g_5000");
		}
		
		gr = "homer";
		for (int i = 0; i < 5; i++) {
			GeneticAlgorithm ga = new GeneticAlgorithm(g, 13);
			ga.setAlgorithmParameters(1000, 2000, 0.1, 0.01, 5);
			ga.run(gr + "_c_1");
			ga = new GeneticAlgorithm(g, 11);
			ga.setAlgorithmParameters(1000, 2000, 0.3, 0.01, 5);
			ga.run(gr + "_c_3");
			ga = new GeneticAlgorithm(g, 11);
			ga.setAlgorithmParameters(1000, 2000, 0.5, 0.01, 5);
			ga.run(gr + "_c_5");
			ga = new GeneticAlgorithm(g, 11);
			ga.setAlgorithmParameters(1000, 2000, 0.7, 0.01, 5);
			ga.run(gr + "_c_7");
		}
		for (int i = 0; i < 5; i++) {
			GeneticAlgorithm ga = new GeneticAlgorithm(g, 13);
			ga.setAlgorithmParameters(1000, 2000, 0.3, 0.01, 5);
			ga.run(gr + "_m_01");
			ga = new GeneticAlgorithm(g, 11);
			ga.setAlgorithmParameters(1000, 2000, 0.3, 0.03, 5);
			ga.run(gr + "_m_03");
			ga = new GeneticAlgorithm(g, 11);
			ga.setAlgorithmParameters(1000, 2000, 0.3, 0.05, 5);
			ga.run(gr + "_m_05");
			ga = new GeneticAlgorithm(g, 11);
			ga.setAlgorithmParameters(1000, 2000, 0.3, 0.1, 5);
			ga.run(gr + "_m_1");
		}
	}

	public static void automatize() {
		String graphFileNames[] = new String[] { "anna", "homer", "queen", "le450_5a", "le450_25d" };
		int colors1[] = new int[] { 11, 13, 13, 5, 25 };
		int colors2[] = new int[] { 15, 15, 15, 10, 30 };
		int colors3[] = new int[] { 20, 20, 20, 20, 40 };
		int colors4[] = new int[] { 30, 30, 30, 40, 80 };

		int colors[][] = new int[][] { colors1, colors2, colors3, colors4 };

		Graph g;
		GeneticAlgorithm ga;
		LargestFirstAlgorithm lf;

		for (int i = 0; i < graphFileNames.length; i++) { // wszystkie pliki
			g = new Graph(graphFileNames[i]);
			for (int j = 0; j < colors.length; j++) {
				ga = new GeneticAlgorithm(g, colors[j][i]);
				ga.run(graphFileNames[i]);
				ga.setAlgorithmParameters(200, 1000, 0.1D, 0.01D, 5);
				ga = new GeneticAlgorithm(g, colors[j][i]);
				ga.run(graphFileNames[i]);
				ga.setAlgorithmParameters(100, 2000, 0.1D, 0.01D, 5);
				ga = new GeneticAlgorithm(g, colors[j][i]);
				ga.run(graphFileNames[i]);
				ga.setAlgorithmParameters(100, 1000, 0.2D, 0.01D, 5);
				ga = new GeneticAlgorithm(g, colors[j][i]);
				ga.run(graphFileNames[i]);
				ga.setAlgorithmParameters(100, 1000, 0.1D, 0.03D, 5);
				ga = new GeneticAlgorithm(g, colors[j][i]);
				ga.run(graphFileNames[i]);
				ga.setAlgorithmParameters(100, 1000, 0.1D, 0.03D, 10);
			}
			lf = new LargestFirstAlgorithm(g);
			lf.run(graphFileNames[i]);
		}

	}

}
