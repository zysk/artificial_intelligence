package org.viking.algorithms.model;

import java.util.Arrays;

import org.apache.commons.lang.ArrayUtils;
import org.viking.generators.RandomGenerator;
import org.viking.structures.graph.Edge;
import org.viking.structures.graph.Graph;

public class Chromosome {

	private int[] genes;
	private int colorCount;
	private Graph graph;

	public int[] getGenes() {
		return genes;
	}
	
	public void setGenes(int[] genes){
		this.genes = genes;
	}

	public Chromosome(Graph g, int colorCount) {
		this.colorCount = colorCount;
		this.graph = g;
		setRandomChromosome();
	}
	
	public Chromosome(Chromosome h){
		this.graph = h.graph;
		this.colorCount = h.colorCount;
		this.genes = new int[graph.getVertexCount()];
	}
	
	public void setRandomChromosome(){
		this.genes = new int[graph.getVertexCount()];
		
		for (int i = 0; i < genes.length; i++) {
			genes[i] = RandomGenerator.getRandomColor(colorCount);
		}
	}
	
	public Chromosome mutate(double probability){
		Chromosome temp = new Chromosome(this);
		for (int i = 0; i < temp.genes.length; i++) {
			if(RandomGenerator.getRandomProbability() < probability){
				temp.genes[i] = RandomGenerator.getRandomColor(colorCount);
			}else{
				temp.genes[i] = genes[i];
			}
		}
		return temp;
	}
	
	public Chromosome coinCross(Chromosome h){
		Chromosome temp = new Chromosome(h);
		for (int i = 0; i < temp.genes.length; i++) {
			if(RandomGenerator.getRandomProbability() >= 0.5){
				temp.genes[i] = h.getGenes()[i];
			}else{
				temp.genes[i] = this.genes[i];
			}
		}
		return temp;
	}
	
	public Chromosome[] mergeCross(Chromosome h){
		Chromosome temp1 = new Chromosome(h);
		Chromosome temp2 = new Chromosome(h);
		
		int[] firstHalf = Arrays.copyOf(genes, genes.length/2);
		int[] secondHalf = Arrays.copyOfRange(h.getGenes(), genes.length/2, genes.length);
		
		if(RandomGenerator.getRandomProbability() >= 0.5){
			temp1.genes = ArrayUtils.addAll(firstHalf, secondHalf);
			temp2.genes = (ArrayUtils.addAll(secondHalf, firstHalf));
		}else{
			temp1.genes = ArrayUtils.addAll(secondHalf, firstHalf);
			temp2.genes = (ArrayUtils.addAll(firstHalf, secondHalf));
		}
		return new Chromosome[] {temp1, temp2};
	}
		
	 
	@Override
	public String toString() {
		return Arrays.toString(genes) + " || " + genes.length;
	}
	
	public int numberOfFaliures(){
		int failures = 0;
		for (Edge edge : graph.getEdges()) {
			if(genes[edge.getBeginVertex()-1] == genes[edge.getEndVertex()-1] ){
				failures++;
			}
		}
		return failures;
	}

}
