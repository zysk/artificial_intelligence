package org.viking.filemanagement;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.viking.algorithms.GeneticAlgorithm;

public class ResultsSaver {

	static BufferedWriter bw = null;
	private static boolean isFooterSaved = false;

	public static void startSaving(String fileName) {
		File resultFile = automatizeFilename(fileName + "-0");

		FileWriter fw;

		try {

			fw = new FileWriter(resultFile.getAbsoluteFile());
			bw = new BufferedWriter(fw);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void saveHeader(int colorCount) {
		try {
			bw.write("<!-- parentPopulationSize=\"" + GeneticAlgorithm.POPULATION_SIZE + "\" ");
			bw.write("generationSize=\"" + GeneticAlgorithm.GENERATION_COUNT + "\" ");
			bw.write("mutationProbability=\"" + GeneticAlgorithm.MUTATION_PROBABILITY + "\" ");
			bw.write("selectionProbability=\"" + GeneticAlgorithm.CROSS_PROBABILITY + "\" ");
			bw.write("tournament=\"" + GeneticAlgorithm.TOURNAMENT + "\" ");
			bw.write("colorCount=\"" + colorCount + "\" --> \n");
			bw.write("<algorithm>\n");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void saveFooter() {
		try {
			isFooterSaved = true;
			bw.write("</algorithm>");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void endSaving() {
		try {
			if(!isFooterSaved){
				saveFooter();
			}

			bw.close();
			isFooterSaved = false;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void saveData(int[] result) {
		try {
			bw.write("\t<generation>\n");
			bw.write("\t\t<number>");
			bw.write(result[0] + "");
			bw.write("</number>\n");
			bw.write("\t\t<best>");
			bw.write(result[1] + "");
			bw.write("</best>\n");
			bw.write("\t\t<worst>");
			bw.write(result[2] + "");
			bw.write("</worst>\n");
			bw.write("\t\t<average>");
			bw.write(result[3] + "");
			bw.write("</average>\n");
			bw.write("\t\t<time>");
			bw.write(result[4] + "");
			bw.write("</time>\n");
			bw.write("\t</generation>\n");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static File automatizeFilename(String fileName) {
		File resultFile = new File("src/main/resources/results/" + fileName + ".xml");

		while (resultFile.exists()) {
			String[] table = fileName.split("-");
			int nr = Integer.parseInt(table[1]) + 1;
			fileName = (table[0] + "-" + nr);
			resultFile = new File("src/main/resources/results/" + fileName + ".xml");
		}

		try {
			resultFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return resultFile;

	}

}
