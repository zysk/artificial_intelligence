package org.viking.algorithms;

import org.viking.algorithms.model.Chromosome;
import org.viking.algorithms.model.Population;
import org.viking.filemanagement.ResultsSaver;
import org.viking.generators.RandomGenerator;
import org.viking.structures.graph.Graph;

public class GeneticAlgorithm {

	private Population population;
	private Graph graph;
	private int colorCount;
	public static int POPULATION_SIZE = 100;
	public static int GENERATION_COUNT = 1000;
	public static double CROSS_PROBABILITY = 0.1D;
	public static double MUTATION_PROBABILITY = 0.01D;
	public static int TOURNAMENT = 10;
	public static long TIME;

	public void setAlgorithmParameters(int populationSize, int generationCount, double crossProbability,
			double mutationProbability, int tournament) {
		POPULATION_SIZE = populationSize;
		GENERATION_COUNT = generationCount;
		CROSS_PROBABILITY = crossProbability;
		MUTATION_PROBABILITY = mutationProbability;
		TOURNAMENT = tournament;
	}

	public GeneticAlgorithm(Graph g, int colorCount) {
		this.graph = g;
		this.colorCount = colorCount;
	}

	public void run(String logFileName) {

		ResultsSaver.startSaving(logFileName);
		ResultsSaver.saveHeader(colorCount);

		population = new Population(graph, POPULATION_SIZE, colorCount);
		population.fillPopulationWithRandomChromosomes();

		Population generation;

		TIME = System.currentTimeMillis();
		try {
			ResultsSaver.saveData(population.evaluate(-1));
		} catch (Exception e) {
			ResultsSaver.endSaving();
			return;
		}

		for (int i = 0; i < GENERATION_COUNT; i++) {
			generation = new Population(graph, POPULATION_SIZE, colorCount);

			while (generation.getActualSize() != POPULATION_SIZE) {

				Chromosome chromosome1 = population.tournamentSelection(TOURNAMENT);
				Chromosome chromosome2 = null;

				if (RandomGenerator.getRandomProbability() < CROSS_PROBABILITY) {
					chromosome2 = population.tournamentSelection(TOURNAMENT);
					if (RandomGenerator.getRandomProbability() > 0.5D) {
						chromosome1 = chromosome1.coinCross(chromosome2);
					} else {
						Chromosome[] mergeCross = chromosome1.mergeCross(chromosome2);
						chromosome1 = mergeCross[0];
						chromosome2 = mergeCross[1];
					}
				}
				chromosome1 = chromosome1.mutate(MUTATION_PROBABILITY);

				if (chromosome2 != null) {
					chromosome2.mutate(MUTATION_PROBABILITY);
				}
				addChromosome(generation, chromosome1);
				addChromosome(generation, chromosome2);
			}

			try {
				ResultsSaver.saveData(generation.evaluate(i));
			} catch (Exception e) {
				ResultsSaver.saveFooter();
				break;
			}
			population = generation;
		}

		ResultsSaver.endSaving();
	}

	private void addChromosome(Population p, Chromosome ch) {

		if (ch != null) {
			if (p.getActualSize() + 1 <= POPULATION_SIZE) {
				p.addChromosome(ch);
			}
		}
	}

}
