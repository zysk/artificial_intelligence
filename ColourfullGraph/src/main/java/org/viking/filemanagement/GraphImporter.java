package org.viking.filemanagement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.viking.structures.graph.Edge;
import org.viking.structures.graph.Graph;

public class GraphImporter {

	private void lineProcessing(String[] splittedLine, Graph g) {
		if (splittedLine[0].equals("e")) {
			int beginVertex = Integer.parseInt(splittedLine[1]);
			int endVertex = Integer.parseInt(splittedLine[2]);
			Edge e = new Edge(beginVertex, endVertex);
			if (!g.getEdges().contains(e))
				g.addEdge(beginVertex, endVertex);
		}
	}

	public void importGraph(File file, Graph g) {

		try {
			@SuppressWarnings("resource")
			BufferedReader fileReader = new BufferedReader(new FileReader(file));

			String line = fileReader.readLine();
			String[] splittedLine;

			while (line != null) {
				splittedLine = line.split(" ");
				lineProcessing(splittedLine, g);
				line = fileReader.readLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

	}

}
