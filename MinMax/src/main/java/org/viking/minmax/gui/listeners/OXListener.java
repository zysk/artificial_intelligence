package org.viking.minmax.gui.listeners;


import org.viking.minmax.games.play.Game;
import org.viking.minmax.games.play.Mode;
import org.viking.minmax.games.play.Move;
import org.viking.minmax.games.ox.OXState;
import org.viking.minmax.gui.OXProjection;
import org.viking.minmax.intelligence.MinMax;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class OXListener extends MouseAdapter {

    private int i, j;
    private OXProjection projection;
    private OXState state;


    public OXListener(int i, int j, OXProjection projection1) {
        this.i = i;
        this.j = j;
        this.projection = projection1;
        state = ((OXState) projection1.getGame().getState());

    }

    @Override
    public void mousePressed(MouseEvent e) {
        switch (projection.getGame().getMode()) {
            case PvP:
                pvpMove();
                break;
            case PvC:
                pvcMove();
                break;
        }


        projection.projectBoard();
        projection.repaintAllLabels();
        isOver();
    }

    private void pvpMove() {
        if (state.getPlayerSwitch()) {
            projection.getGame().getOriginal().makeMove(new Move(i, j));
        } else {
            projection.getGame().getOpponent().makeMove(new Move(i, j));
        }
        projection.removeMouseListenerFormPanel(i, j, this);
    }

    private void pvcMove() {
        if (state.getPlayerSwitch()) {
            projection.getGame().getOriginal().makeMove(new Move(i, j));
            projection.removeMouseListenerFormPanel(i, j, this);
            projection.projectBoard();
            projection.repaintAllLabels();


            MinMax minmax = new MinMax(3);
            Move m = minmax.bestMove(projection.getGame().getState(),
                    projection.getGame().getOpponent(),
                    projection.getGame().getOriginal());

            projection.getGame().getOpponent().makeMove(m);
            projection.removeMouseListenerFormPanel(m.getX(), m.getY(), this);
        }
    }


    private boolean isOver() {
        if (state.isOver()) {
            JOptionPane.showMessageDialog(projection, "Game over!");

            projection.startOXProjection(new Game(new OXState(
                    state.getSIZE(),
                    state.getCheckSize())));
            projection.projectBoard();
            projection.repaintAllLabels();

            return true;
        }
        return false;
    }


}
