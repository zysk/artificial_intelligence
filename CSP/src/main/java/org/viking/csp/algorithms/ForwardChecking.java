package org.viking.csp.algorithms;

import java.util.Arrays;

import org.viking.csp.model.AbstractQueen;

public class ForwardChecking extends AbstractQueen {

	private boolean[][] domain;
	int[] removedQueen;

	public ForwardChecking(int SIZE) {
		super(SIZE);
		domain = new boolean[SIZE][SIZE];
		removedQueen = new int[2];
	}

	private void cleanDomain() {
		for (int i = 0; i < board.length; i++) {
			Arrays.fill(domain[i], false);
		}
	}

	@Override
	public boolean run(int column) {
		boolean success = false;
		int row = -1;

		while (!success && (row = getFirstAvailable(column, row)) >= 0) {
			iterationCount++;
			
			if (column == SIZE - 1) {
				resultCount++;
			}
			
			putQueen(row, column);
			success = run(column + 1);

			if (!success) {
				removeQueen(row, column);
			}
		}
		return success;

	}

	@Override
	public void putQueen(int row, int column) {
		board[row][column] = true;
		updateDomain();
	}

	@Override
	public void removeQueen(int row, int column) {
		removedQueen[0] = row;
		removedQueen[1] = column;
		board[row][column] = false;
		updateDomain();
		updateDomainWithRemovedQueen();
	}

	private int getFirstAvailable(int column, int row) {
		if (column < SIZE) {
			for (int i = row + 1; i < SIZE; i++) {
				if (!domain[i][column])
					return i;
			}
		}
		return -1;
	}

	private void updateDomain() {
		cleanDomain();
		for (int row = 0; row < SIZE; row++) {
			for (int col = 0; col < SIZE; col++) {
				updateDomainField(row, col);
			}
		}
	}

	private void updateDomainWithRemovedQueen() {
		domain[removedQueen[0]][removedQueen[1]] = true;
	}

	private void updateDomainField(int row, int col) {
		if (board[row][col] == true) {
			updateRow(row, col);
			updateDownDiagonal(row, col);
			updateUpDiagonal(row, col);
		}
	}

	private void updateDownDiagonal(int row, int col) {
		while (col < SIZE - 1 && row < SIZE - 1) {
			domain[++row][++col] = true;
		}
	}

	private void updateUpDiagonal(int row, int col) {
		while (col < SIZE - 1 && row > 0) {
			domain[--row][++col] = true;
		}
	}

	private void updateRow(int row, int col) {
		for (int i = col; i < SIZE; i++) {
			domain[row][i] = true;
		}
	}

	public void printDomain() {
		System.out.println("----DOMAIN----");
		for (int row = 0; row < SIZE; row++) {
			for (int col = 0; col < SIZE; col++) {
				System.out.print(domain[row][col] == true ? "1 " : "0 ");
			}
			System.out.println();
		}
	}

}