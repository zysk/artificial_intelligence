package org.viking.csp.runtime;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.viking.csp.algorithms.BackTracking;
import org.viking.csp.algorithms.ForwardChecking;

public class AlgorithmRunner {

	private File resultFile;
	private BackTracking bt;
	private ForwardChecking fc;
	private String[] rows = new String[] { "N;", "BT_Results_Count;",
			"BT_Iteration_Count;", "BT_Duration;", "FC_Results_Count;",
			"FC_Iteration_Count;", "FC_Duration;" };

	public AlgorithmRunner(String fileName) {
		resultFile = new File(fileName + ".csv");
		createFile();
		cleanFile();
	}

	private void createFile() {
		try {
			if (!resultFile.exists()) {
				resultFile.createNewFile();
			} else {
				FileUtils.forceDelete(resultFile);
				createFile();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void cleanFile() {
		try {
			for (int i = 0; i < rows.length; i++) {
				FileUtils.writeStringToFile(resultFile, rows[i], true);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void saveResults(int size) {
		for (int N = 1; N <= size; N++) {
			bt = new BackTracking(N);
			bt.run();
			fc = new ForwardChecking(N);
			fc.run();
			appendResultsToFile(N);
		}
	}

	private void appendResultsToFile(int N) {
		String toAppend = "\n" + String.valueOf(N) + ";" + bt.getResultCount()
				+ ";" + bt.getIterationCount() + ";" + bt.getDuration() + ";"
				+ fc.getResultCount() + ";" + fc.getIterationCount() + ";"
				+ fc.getDuration() + ";";
		try {
			FileUtils.writeStringToFile(resultFile, toAppend, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
