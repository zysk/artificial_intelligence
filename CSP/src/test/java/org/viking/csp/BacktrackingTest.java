package org.viking.csp;
import org.junit.Test;
import static org.junit.Assert.*;
import org.viking.csp.algorithms.BackTracking;

public class BacktrackingTest {

	BackTracking bc;
	
	@Test
	public void shoudBeConflictOnRow(){
		bc = new BackTracking(4);
		bc.putQueen(0, 0);
		assertTrue(bc.isConflictOnRow(0,3));
	}
	
	@Test
	public void shoudNotBeConflictOnRow(){
		bc = new BackTracking(4);
		bc.putQueen(0, 0);
		assertFalse(bc.isConflictOnRow(1,3));
	}
	
	@Test
	public void shoudBeConflictOnUpDiagonal(){
		bc = new BackTracking(4);
		bc.putQueen(0, 0);
		assertTrue(bc.isConflictOnUpDiagonal(3,3));
	}
	
	@Test
	public void shoudNotBeConflictOnUpDiagonal(){
		bc = new BackTracking(4);
		bc.putQueen(0, 0);
		assertFalse(bc.isConflictOnUpDiagonal(2,3));
	}
	
	@Test
	public void shoudBeConflictOnDownDiagonal(){
		bc = new BackTracking(4);
		bc.putQueen(2,0);
		assertTrue(bc.isConflictOnDownDiagonal(0,2));
	}
	
	@Test
	public void shoudNotBeConflictDownDiagonal(){
		bc = new BackTracking(4);
		bc.putQueen(0, 0);
		assertFalse(bc.isConflictOnDownDiagonal(2,3));
	}
	
}
