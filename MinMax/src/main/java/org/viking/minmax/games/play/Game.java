package org.viking.minmax.games.play;

import org.viking.minmax.games.ox.OXFigure;
import org.viking.minmax.games.players.Player;
import org.viking.minmax.games.players.impl.User;
import org.viking.minmax.model.Board;


public class Game implements Cloneable {

    private Mode mode;
    private Board state;
    private Player original;
    private Player opponent;


    public Game(Board state) {
        this.state = state;
        mode = Mode.PvC;
        this.original = new User(state, OXFigure.O);
        this.opponent = new User(state, OXFigure.X);
    }

    public Game(Board state, Mode mode) {
        this.state = state;
        this.mode = mode;
        this.original = new User(state, OXFigure.O);
        this.opponent = new User(state, OXFigure.X);
    }


    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public Board getState() {
        return state;
    }


    public boolean isOver() {
        return state.isOver();
    }


    public void setOriginal(Player original) {
        this.original = original;
    }

    public void setOpponent(Player opponent) {
        this.opponent = opponent;
    }


    public Player getOpponent() {
        return opponent;
    }


    public Player getOriginal() {
        return original;
    }

    @Override
    public Object clone() {
        Game clone = new Game((Board) state.clone());
        clone.setOpponent((Player) opponent.clone());
        clone.setOriginal((Player) original.clone());
        return clone;
    }
}

