package org.viking.minmax.games.play;

public enum Mode {

    PvP, PvC, CvC;

}
