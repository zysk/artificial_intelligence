package org.viking.algorithms.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.viking.algorithms.GeneticAlgorithm;
import org.viking.exceptions.AlgorithmFoundSolutionException;
import org.viking.filemanagement.ResultsSaver;
import org.viking.generators.RandomGenerator;
import org.viking.structures.graph.Graph;

public class Population {

	private List<Chromosome> population;
	private int populationSize;
	private Graph graph;
	private int colorCount;

	public Population(Graph g, int populationSize, int colorCount) {
		population = new ArrayList<Chromosome>();
		this.graph = g;
		this.populationSize = populationSize;
		this.colorCount = colorCount;
	}

	public int getPopulationSize() {
		return populationSize;
	}

	public int getActualSize() {
		return population.size();
	}

	public Chromosome tournamentSelection(int tournamentSize) {
		HashMap<Integer, Chromosome> tournament = new HashMap<Integer, Chromosome>();
		Chromosome choosenOne;
		int failures = 0;
		int bestFailuresCount = Integer.MAX_VALUE;

		for (int i = 0; i < tournamentSize; i++) {

			choosenOne = population.get(RandomGenerator.getRandomInteger(populationSize - 1));
			failures = choosenOne.numberOfFaliures();
			tournament.put(failures, choosenOne);

			if (failures < bestFailuresCount) {
				bestFailuresCount = failures;
			}
		}

		return tournament.get(bestFailuresCount);

	}

	public void addChromosome(Chromosome h) {
		population.add(h);
	}

	public int size() {
		return population.size();
	}

	public void fillPopulationWithRandomChromosomes() {
		population.clear();
		for (int i = 0; i < populationSize; i++) {
			population.add(new Chromosome(graph, colorCount));
		}
	}

	public int[] evaluate(int generation) throws AlgorithmFoundSolutionException {
		int best = Integer.MAX_VALUE;
		int worst = Integer.MIN_VALUE;
		int average = 0;
		int currentFails = 0;

		for (Chromosome chromosome : population) {
			currentFails = chromosome.numberOfFaliures();
			if (currentFails < best) {
				best = currentFails;
			}
			if (currentFails > worst) {
				worst = currentFails;
			}
			average += currentFails;
		}

		System.out.println("generation: " + generation + ", best: " + best + ", worse: " + worst
				+ ", average: " + average / GeneticAlgorithm.POPULATION_SIZE + ", time: "
				+ (System.currentTimeMillis() - GeneticAlgorithm.TIME));

		if (best == 0) {
			ResultsSaver.saveData(new int[] { generation, best, worst,
					average / GeneticAlgorithm.POPULATION_SIZE, (int) (System.currentTimeMillis() - GeneticAlgorithm.TIME) });
			throw new AlgorithmFoundSolutionException();
		}
		
		return new int[] { generation, best, worst, average / GeneticAlgorithm.POPULATION_SIZE, (int) (System.currentTimeMillis() - GeneticAlgorithm.TIME) };

	}

	public void printPopulation() {
		for (int i = 0; i < getActualSize(); i++)
			System.out.println(population.get(i).toString());
	}

	// TODO cho na uszko - Madzioszek
}
