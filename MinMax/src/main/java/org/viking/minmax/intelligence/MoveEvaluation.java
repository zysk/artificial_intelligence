package org.viking.minmax.intelligence;


import org.viking.minmax.games.play.Move;

public class MoveEvaluation {

    private Move move;
    private int eval;

    public Move getMove() {
        return move;
    }

    public void setMove(Move move) {
        this.move = move;
    }

    public int getEval() {
        return eval;
    }

    public void setEval(int eval) {
        this.eval = eval;
    }

    public MoveEvaluation(Move move, int eval) {
        this.move = move;
        this.eval = eval;
    }

    public MoveEvaluation(int eval) {
        this.eval = eval;
    }
}
