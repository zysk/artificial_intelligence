package org.viking.minmax.gui;

import org.viking.minmax.games.play.Game;
import org.viking.minmax.games.ox.OXState;
import org.viking.minmax.games.play.Mode;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

public class Window extends JFrame {

    private BorderLayout layout;
    OXProjection projection;
    private JMenuBar menuBar;
    private JMenu oxMenu;
    private JMenu playersMenu;
    private JMenuItem selectMode;
    private JMenuItem oxNewProjection;
    private JMenuItem oxParams;
    private Mode mode = Mode.PvC;


    public Window() {
        super("Mariusz Zyśk - MinMax");
        layout = new BorderLayout();
        this.setLayout(layout);
        projection = new OXProjection(new Game(new OXState(5, 4)));
        startupConfiguration();
        setLookAndFeel();
        add(BorderLayout.CENTER, projection);
        setMenu();

        this.invalidate();
        this.validate();

    }

    private void setMenu() {
        menuBar = new JMenuBar();
        playersMenu = new JMenu("PLAYERS");
        oxMenu = new JMenu("OX");
        oxNewProjection = new JMenuItem("New Game");
        oxParams = new JMenuItem("Change Paramethers");
        selectMode = new JMenuItem("Set game mode");

        oxNewProjection.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
        oxParams.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
        selectMode.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK));

        oxNewProjection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                projection.startOXProjection(new Game(new OXState(), mode));
            }
        });

        oxParams.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String[] levels = {"Easy", "Medium", "Gomoku"};
                int result = JOptionPane.showOptionDialog(null,
                        "Choose level:", "OX Level",
                        JOptionPane.OK_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null, levels, levels[0]);

                switch (result) {
                    case 0:
                        projection.startOXProjection(new Game(new OXState(), mode));
                        break;
                    case 1:
                        projection.startOXProjection(new Game(new OXState(5, 4), mode));
                        break;
                    case 2:
                        projection.startOXProjection(new Game(new OXState(19, 5), mode));
                        break;
                }
            }
        });

        selectMode.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Mode[] levels = {Mode.PvP, Mode.PvC, Mode.PvC};
                int result = JOptionPane.showOptionDialog(null,
                        "Choose game mode:", "Game mode",
                        JOptionPane.OK_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null, levels, levels[0]);

                mode = levels[result];
                projection.startOXProjection(new Game(new OXState(19, 5), mode));
            }
        });

        oxMenu.add(oxNewProjection);
        oxMenu.add(oxParams);
        playersMenu.add(selectMode);
        menuBar.add(playersMenu);
        menuBar.add(oxMenu);
        this.setJMenuBar(menuBar);
        menuBar.repaint();
    }

    private void startupConfiguration() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
        setSize(600, 600);
        setResizable(true);
        setLocationRelativeTo(null);
        setLookAndFeel();
    }

    private void setLookAndFeel() {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
