package org.viking.minmax.model;


public interface Board extends Cloneable {

    int getCheckSize();
    void setCheckSize(int checkSize);
    int getSIZE();
    void putFigure(Figure figure, int row, int column);
    void removeFigure(int row, int column);
    Figure[][] getBoard();
    boolean isOver();
    boolean hasEmptyFields();
    Object clone();
    void switchPlayer();
}
