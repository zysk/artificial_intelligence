package org.viking.minmax.intelligence.comparators;

public class MinComparator implements  MinMaxComparator {

    @Override
    public int initalValue() {
        return Integer.MAX_VALUE;
    }

    @Override
    public MinMaxComparator opposite() {
        return MinMaxComparator.MAX;
    }

    @Override
    public int compare(Integer o1, Integer o2) {
        if(o1 > o2) return -1;
        if(o1 == o2) return 0;
        return 1;
    }
}
